# Prometheus time series database extractor
There is a need to be able to extract a data from prometheus tsdb. Currently I did not find any suitable solution to get a job done.
Thus I began this project.

## Introduction
As part of my daily work I had a need to somehow read prometheus data. I've googled and found nothing to work with.
Next step, I decided to implement basic functionality myself.

It looks:
data
snapshots
01CE8WWGM6B679ENQQTVG83820
   chunks
       000001 -- is binary data
   meta.json  
   tombstones
   index
...
wal
01CENJ0CNG59BTDHZ6JMDWJ0G2



## TSDB format
It consists of three components: index, chunks and tombstones.

```
┌────────────────────────────┬─────────────────────┐
│ magic(0xBAAAD700) <4b>     │ version(1) <1 byte> │
├────────────────────────────┴─────────────────────┤
│ ┌──────────────────────────────────────────────┐ │
│ │                 Symbol Table                 │ │
│ ├──────────────────────────────────────────────┤ │
│ │                    Series                    │ │
│ ├──────────────────────────────────────────────┤ │
│ │                 Label Index 1                │ │
│ ├──────────────────────────────────────────────┤ │
│ │                      ...                     │ │
│ ├──────────────────────────────────────────────┤ │
│ │                 Label Index N                │ │
│ ├──────────────────────────────────────────────┤ │
│ │                   Postings 1                 │ │
│ ├──────────────────────────────────────────────┤ │
│ │                      ...                     │ │
│ ├──────────────────────────────────────────────┤ │
│ │                   Postings N                 │ │
│ ├──────────────────────────────────────────────┤ │
│ │               Label Index Table              │ │
│ ├──────────────────────────────────────────────┤ │
│ │                 Postings Table               │ │
│ ├──────────────────────────────────────────────┤ │
│ │                      TOC                     │ │
│ └──────────────────────────────────────────────┘ │
└──────────────────────────────────────────────────┘
```
** TOC ** table of content
### Symbol table
```
┌────────────────────┬─────────────────────┐
│ len <4b>           │ #symbols <4b>       │
├────────────────────┴─────────────────────┤
│ ┌──────────────────────┬───────────────┐ │
│ │ len(str_1) <uvarint> │ str_1 <bytes> │ │
│ ├──────────────────────┴───────────────┤ │
│ │                . . .                 │ │
│ ├──────────────────────┬───────────────┤ │
│ │ len(str_n) <uvarint> │ str_n <bytes> │ │
│ └──────────────────────┴───────────────┘ │
├──────────────────────────────────────────┤
│ CRC32 <4b>                               │
└──────────────────────────────────────────┘
```
The **symbol table** holds a sorted list of deduplicated strings that occurred in label pairs of the stored series. They can be referenced from subsequent sections and significantly reduce the total index size. 
The section contains a sequence of the string entries, each prefixed with the string's length in raw bytes. All strings are utf-8 encoded. Strings are referenced by sequential indexing. The strings are sorted in lexicographically ascending order.




### Chunks Disk Format
The following describes the format of a single chunks file, which is created in the chunks/ directory of a block. The maximum size per segment file is 512MiB.

```
┌────────────────────────────────────────┬──────────────────────┐
│ magic(0x85BD40DD) <4 byte>             │ version(1) <1 byte>  │
├────────────────────────────────────────┴──────────────────────┤
│ ┌───────────────┬───────────────────┬──────┬────────────────┐ │
│ │ len <uvarint> │ encoding <1 byte> │ data │ CRC32 <4 byte> │ │
│ └───────────────┴───────────────────┴──────┴────────────────┘ │
└───────────────────────────────────────────────────────────────┘

```

### Tombstones Disk Format
The following describes the format of a tombstones file, which is placed at the top level directory of a block. The last 8 bytes specifies the offset to the start of Stones section. The stones section is 0 padded to a multiple of 4 for fast scans.

```
┌────────────────────────────┬─────────────────────┐
│ magic(0x0130BA30) <4 byte> │ version(1) <1 byte> │
├────────────────────────────┴─────────────────────┤
│ ┌──────────────────────────────────────────────┐ │
│ │                Tombstone 1                   │ │
│ ├──────────────────────────────────────────────┤ │
│ │                      ...                     │ │
│ ├──────────────────────────────────────────────┤ │
│ │                Tombstone N                   │ │
│ ├──────────────────────────────────────────────┤ │
│ │                  CRC<4b>                     │ │
│ └──────────────────────────────────────────────┘ │
└──────────────────────────────────────────────────┘

```

## Cyclic Checksum
Cyclic Redundancy Checks polynomial arithmetic are used for error detection in many data transmission and storage applications.
The polynomial is just one of many parameters which must match in order for a CRC implementation to interoperate with existing systems:
* the byte-order and bit-order of the data stream; 
* whether the CRC or its inverse is being calculated; 
* the initial CRC value; and 
* whether and where the CRC value is appended (inverted or non-inverted) to the data stream.

```
Function: crc:make-table poly 
```
** poly ** must be string of ‘1’s and ‘0’s beginning with ‘1’ and having length greater than 8. crc:make-table returns a vector of 256 integers, such that:

```
(set! crc
                (logxor (ash (logand (+ -1 (ash 1 (- deg 8))) crc) 8)
                        (vector-ref crc-table
                                    (logxor (ash crc (- 8 deg)) byte))))
```

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
