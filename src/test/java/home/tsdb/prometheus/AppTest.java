package home.tsdb.prometheus;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import home.tsdb.prometheus.dto.IndexTOC;
import home.tsdb.prometheus.dto.SymbolTable;
import home.tsdb.prometheus.dto.Varint;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testChunk() {
		try {
			// Chunk
			Path path = Paths.get("../tsdb.prometheus/src/test/resources/chunk");
			String magicNumber = "85BD40DD";
			byte[] magicNumberAsArray= hexStringToByteArray(magicNumber); 
			byte[] fileContents =  Files.readAllBytes(path);
			//len <uvarint> │ encoding <1 byte> │ data │ CRC32 <4 byte>
			byte[] tmp = Arrays.copyOfRange(fileContents, 6, 10);
			long v = Converters.bytesToLong(tmp);
			byte[] toStr = Arrays.copyOfRange(fileContents, 10, (int)v);
			System.out.println(new String(toStr));
			
			byte[] slice = Arrays.copyOfRange(fileContents, 0, 4);
			if(Arrays.equals(magicNumberAsArray, slice)) {
				System.out.println(Arrays.toString(slice));
			}
            			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void testIndex() {
		try {
			// index
			Path path = Paths.get("../tsdb.prometheus/src/test/resources/index");
			String magicNumberIndex = "BAAAD700";
			byte[] magicNumberAsArray= hexStringToByteArray(magicNumberIndex); 
			byte[] fileContents =  Files.readAllBytes(path);
			
			IndexTOC indexTOC = IndexTOC.from(fileContents);
			System.out.println(indexTOC);
			
			SymbolTable symbolTable = SymbolTable.from(fileContents);
			System.out.println(symbolTable);
			
			byte[] slice = Arrays.copyOfRange(fileContents, 0, 4);
			if(Arrays.equals(magicNumberAsArray, slice)) {
				System.out.println(Arrays.toString(slice));
			}
			
			
            			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void testTombstones() {
		try {
			// tombstones
			Path path = Paths.get("../tsdb.prometheus/src/test/resources/tombstones");
			String magicNumberIndex = "0130BA30";
			byte[] magicNumberAsArray= hexStringToByteArray(magicNumberIndex); 
			byte[] fileContents =  Files.readAllBytes(path);
			
			byte[] slice = Arrays.copyOfRange(fileContents, 0, 4);
			
			Checksum checksum = new CRC32();
			checksum.update(fileContents, 0, fileContents.length);
			System.out.println(checksum.getValue());
			
			System.out.println(bytesToHex(slice));
			if(Arrays.equals(magicNumberAsArray, slice)) {
				System.out.println(Arrays.toString(slice));
			}
			
			
            			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}
	
	public static String bytesToHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		for (byte b : bytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}
}
