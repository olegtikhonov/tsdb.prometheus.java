package home.tsdb.prometheus;

import java.math.BigInteger;
import java.security.SecureRandom;
import home.tsdb.prometheus.dto.Label;


public class TestUtils {
	private static final SecureRandom random = new SecureRandom();
	
	private TestUtils() {}
	
	
	public static String nextString() {
		return new BigInteger(130, random).toString(32);
	}
	
	public static Label build() {
		return new Label(nextString(), nextString());
	}

}
