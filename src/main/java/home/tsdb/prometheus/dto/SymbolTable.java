package home.tsdb.prometheus.dto;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import home.tsdb.prometheus.Converters;

/**
 * The symbol table holds a sorted list of deduplicated strings that occurred in label pairs of the stored series. 
 * They can be referenced from subsequent sections and significantly reduce the total index size. The section contains 
 * a sequence of the string entries, each prefixed with the string's length in raw bytes. All strings are utf-8 encoded. Strings 
 * are referenced by sequential indexing. The strings are sorted in lexicographically ascending order.
 * 
 * Goes after magic + version
 */
public class SymbolTable {
	// len <4b>
	private int symbolAmmount;
	private List<StringEntry> symbols;
	private static final int START = 6;
	private static final int INT_STEP = 4;
	
	public SymbolTable() { 
		symbols = new LinkedList<>();
	}
	
	
	public int getSymbolAmmount() {
		return symbolAmmount;
	}
	
	
	public void setSymbolAmmount(int symbolAmmount) {
		this.symbolAmmount = symbolAmmount;
	}
	
	public List<StringEntry> getSymbols() {
		return symbols;
	}


	public static SymbolTable from(byte[] rawData) {
		SymbolTable local = new SymbolTable();
		
		int currentIndex = START;
		local.setSymbolAmmount(Converters.bytesToInt(Arrays.copyOfRange(rawData, currentIndex, currentIndex + INT_STEP)));
		
		// NEXT_RAW_BYTES ...
		currentIndex = currentIndex + INT_STEP;
		
		int theOffset = Varint.readUnsignedVarInt(Arrays.copyOfRange(rawData, currentIndex, currentIndex + INT_STEP));//Converters.bytesToInt(Arrays.copyOfRange(rawData, currentIndex, currentIndex + INT_STEP));
		
		currentIndex = currentIndex + INT_STEP;
		StringEntry tmp;
		try {
			tmp = new StringEntry.Builder().entryLenght(theOffset).symbol(new String(Arrays.copyOfRange(rawData, currentIndex, theOffset), "UTF-8")).build();
			local.getSymbols().add(tmp);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		
		return local;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append(" [symbolAmmount=");
		builder.append(symbolAmmount);
		builder.append(", symbols=");
		builder.append(symbols);
		builder.append("]");
		return builder.toString();
	}

}
