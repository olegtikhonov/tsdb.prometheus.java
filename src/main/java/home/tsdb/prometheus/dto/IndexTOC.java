package home.tsdb.prometheus.dto;


import java.util.Arrays;
import java.util.PrimitiveIterator;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import home.tsdb.prometheus.Converters;
import home.tsdb.prometheus.validation.ValidatorHelder;

/**
 * type indexTOC struct {
 *	symbols           uint64
 *	series            uint64
 *	labelIndices      uint64
 *	labelIndicesTable uint64
 *	postings          uint64
 *	postingsTable     uint64
 * }
 *
 */
public class IndexTOC {
	// TODO: currently expected checksum != currect checksum
	// ref(symbols) <8b> 
	private long symbols;
	// ref(series) <8b>    
	private long series;
	// ref(label indices start) <8b>  
	private long labelIndices;
	// ref(label indices table) <8b> 
	private long labelIndicesTable;
	// ref(postings start) <8b>  
	private long postings;
	// ref(postings table) <8b>
    private long postingsTable;
    private static Checksum crc32;
    private long checksum;    
    private static final int INDEX_TOC_LEN = 6*8 + 4;
	private static final int STEP = 8;

//	static final Logger logger = LogManager.getLogger(IndexTOC.class.getName());
	
	/**
	 * Default citor.
	 */
	public IndexTOC() {
		crc32 = new CRC32();
	}
	
    
    /**
     * 
     * @param symbols
     * @param series
     * @param labelIndices
     * @param labelIndicesTable
     * @param postings
     * @param postingsTable
     */
    public IndexTOC(long symbols, long series, long labelIndices, long labelIndicesTable, long postings, long postingsTable) {
		this.symbols = symbols;
		this.series = series;
		this.labelIndices = labelIndices;
		this.labelIndicesTable = labelIndicesTable;
		this.postings = postings;
		this.postingsTable = postingsTable;
	}

	public long getSymbols() {
		return symbols;
	}

	public void setSymbols(long symbols) {
		this.symbols = symbols;
	}

	public long getSeries() {
		return series;
	}

	public void setSeries(long series) {
		this.series = series;
	}

	public long getLabelIndices() {
		return labelIndices;
	}

	public void setLabelIndices(long labelIndices) {
		this.labelIndices = labelIndices;
	}

	public long getLabelIndicesTable() {
		return labelIndicesTable;
	}

	public void setLabelIndicesTable(long labelIndicesTable) {
		this.labelIndicesTable = labelIndicesTable;
	}

	public long getPostings() {
		return postings;
	}

	public void setPostings(long postings) {
		this.postings = postings;
	}

	public long getPostingsTable() {
		return postingsTable;
	}

	public void setPostingsTable(long postingsTable) {
		this.postingsTable = postingsTable;
	}
	
	public long getChecksum() {
		return checksum;
	}

	public void setChecksum(long checksum) {
		this.checksum = checksum;
	}
	
	


	public Checksum getCrc32() {
		return crc32;
	}


	public static IndexTOC from(byte[] rawData) {
		IndexTOC local = new IndexTOC();
		int lastIndex = rawData.length;
		int currentIndex = lastIndex - INDEX_TOC_LEN;
		
		local.setPostingsTable(Converters.bytesToLong(Arrays.copyOfRange(rawData, currentIndex, currentIndex + STEP)));
		
		currentIndex = currentIndex + STEP;
		local.setPostings(Converters.bytesToLong(Arrays.copyOfRange(rawData, currentIndex, currentIndex + STEP)));
		
		currentIndex = currentIndex + STEP;
		local.setLabelIndicesTable(Converters.bytesToLong(Arrays.copyOfRange(rawData, currentIndex, currentIndex + STEP)));
		
		currentIndex = currentIndex + STEP;
		local.setLabelIndices(Converters.bytesToLong(Arrays.copyOfRange(rawData, currentIndex, currentIndex + STEP)));
		
		currentIndex = currentIndex + STEP;
		local.setSeries(Converters.bytesToLong(Arrays.copyOfRange(rawData, currentIndex, currentIndex + STEP)));
		
		currentIndex = currentIndex + STEP;
		local.setSymbols(Converters.bytesToLong(Arrays.copyOfRange(rawData, currentIndex, currentIndex + STEP)));
		
		// TODO: validate checksum expected == current
		crc32.update(rawData, 0, rawData.length - 4);
		long expCRC = crc32.getValue();
		
		crc32.reset();
		currentIndex = currentIndex + STEP;
		byte[] tmpChecksum = Arrays.copyOfRange(rawData, currentIndex, currentIndex + 4); 
		crc32.update(tmpChecksum, 0,  tmpChecksum.length);
		
		local.setChecksum(crc32.getValue());
		System.out.println(ValidatorHelder.isValid(expCRC, local.getChecksum()));
		
		return local;
	}
	
	class IndexTOCIterator implements PrimitiveIterator<byte[], ByteArrayConsumer> {
        private byte[] input;
        private int step;
        volatile private int currentIndex;
		
		IndexTOCIterator(byte[] input, int step) {
			this.input = input;
			this.step = step;
		}
		
		@Override
		public boolean hasNext() {
			return ((input != null) && (input.length >= (currentIndex + step)));
		}

		@Override
		public byte[] next() {
			byte[] tmp = Arrays.copyOfRange(input, currentIndex, step);
			currentIndex = currentIndex + step;
			return tmp;
		}

		@Override
		public void forEachRemaining(ByteArrayConsumer action) {
			action.accept(input);
		}

	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append(" [symbols=");
		builder.append(symbols);
		builder.append(", series=");
		builder.append(series);
		builder.append(", labelIndices=");
		builder.append(labelIndices);
		builder.append(", labelIndicesTable=");
		builder.append(labelIndicesTable);
		builder.append(", postings=");
		builder.append(postings);
		builder.append(", postingsTable=");
		builder.append(postingsTable);
		builder.append(", checksum=");
		builder.append(checksum);
		builder.append(']');
		return builder.toString();
	}
}
