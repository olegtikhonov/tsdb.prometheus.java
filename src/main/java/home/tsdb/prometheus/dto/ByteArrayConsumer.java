package home.tsdb.prometheus.dto;

public interface ByteArrayConsumer {
	void accept(byte[] value);
}
