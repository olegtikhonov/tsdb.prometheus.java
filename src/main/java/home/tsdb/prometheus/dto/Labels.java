package home.tsdb.prometheus.dto;


import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Label is a key/value pair of strings.
 * Labels is a sorted set of labels. Order has to be guaranteed upon instantiation.
 */
public class Labels {
    private List<Label> labels;
    
    public Labels() {
    	labels = new LinkedList<>();
    }
    
    
    public int len() {
    	return labels.size();
    }
    
    public void swap(int i, int j ) { 
    	Label tmp = labels.get(j);
    	labels.add(j,labels.get(i));
    	labels.add(i,tmp);
    }
    
    public boolean less(int i, int j) {
    	return (labels.get(i).getName().compareTo(labels.get(j).getName()) < 0) ? true : false; 
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append('{');
		labels.forEach(item -> {builder.append(item.getName()).append('=').append(item.getValue()).append(',');});
		builder.append('}');

		return builder.toString();
	}
	
	public void add(Label item) {
		labels = Stream.concat(labels.stream(), Stream.of(item)).collect(Collectors.toList());
	}
	
	public List<Label> get() {
		Collections.sort(labels, new LabelComparator());
		return this.labels;
	}
	

	class LabelComparator implements Comparator<Label> {
		@Override
		public int compare(Label o1, Label o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
}
