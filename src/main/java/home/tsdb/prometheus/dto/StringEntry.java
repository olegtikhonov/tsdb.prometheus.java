package home.tsdb.prometheus.dto;


/**
 * The section contains a sequence of the string entries, each prefixed with the string's length in raw bytes. All strings are utf-8 encoded. 
 * Strings are referenced by sequential indexing. The strings are sorted in lexicographically ascending order.
 */
public class StringEntry {
	private int entryLenght;
	private String symbol;
	

	public static class Builder {
		private int entryLenght;
		private String symbol;
		
		public Builder entryLenght(int entryLenght) {
			this.entryLenght = entryLenght;
			return this;
		}
		
		public Builder symbol(String symbol) {
			this.symbol = symbol;
			return this;
		}
		
        public StringEntry build() {
            return new StringEntry(this);
        }
	}
	
	private StringEntry(Builder builder) {
		this.entryLenght = builder.entryLenght;
		this.symbol = builder.symbol;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append(" [entryLenght=");
		builder.append(entryLenght);
		builder.append(", symbol=");
		builder.append(symbol);
		builder.append("]");
		
		return builder.toString();
	}
}
