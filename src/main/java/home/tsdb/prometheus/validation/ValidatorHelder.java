package home.tsdb.prometheus.validation;

import java.util.function.BiPredicate;


/**
 * Does validation
 *
 */
public final class ValidatorHelder {
    /* Checksum checker */	
	static BiPredicate<Long, Long> checksumChecker = (first, second) -> first.equals(second);
	
	public static ValidationResult isValid(long expected, long current) {
		return (checksumChecker.test(expected, current)) ? ValidationResult.ok() : ValidationResult.fail("Invalid Checksum");
	}

}
