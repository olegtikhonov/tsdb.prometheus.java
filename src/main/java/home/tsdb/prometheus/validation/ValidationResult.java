package home.tsdb.prometheus.validation;

public class ValidationResult {
	private boolean valid;
	private String messsage;

	
	private ValidationResult(boolean valid, String messsage) {
		this.valid = valid;
		this.messsage = messsage;
	}
	
	
	public static ValidationResult ok() {
		return new ValidationResult(true, null);
	}

	public static ValidationResult fail(String message) {
		return new ValidationResult(false, message);
	}


	public boolean isvalid() {
		return valid;
	}

	public void throwIfInvalid() {
		if (!isvalid())
			throw new IllegalArgumentException(getMesssage());
	}

	public void throwIfInvalid(String fieldName) {
		if (!isvalid())
			throw new IllegalArgumentException(fieldName + " : " + getMesssage());
	}

	public String getMesssage() {
		return messsage;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append(" [valid=");
		builder.append(valid);
		builder.append(", messsage=");
		builder.append(messsage);
		builder.append("]");
		
		return builder.toString();
	}
}
