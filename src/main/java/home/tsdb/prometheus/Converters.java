package home.tsdb.prometheus;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;


public final class Converters {
    private Converters() {}
    
    
    /**
     * Converts long to byte[].
     * 
     * @param x
     * @return
     */
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putLong(x);
        return buffer.array();
    }

    /**
     * Converts byte[] to long
     * 
     * @param bytes
     * @return
     */
    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.put(bytes);
        buffer.flip(); 
        return buffer.getLong();
    }
    
    /**
     * Converts byte[] to int.
     * 
     * @param bytes
     * @return
     */
    public static int bytesToInt(byte[] bytes) {
    	System.out.println(Arrays.toString(bytes));
    	// big endian by default
    	// ByteBuffer.wrap(array);
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.put(bytes);
        buffer.flip(); 
        return buffer.getInt();
    }
    
    /**
     * Converts int to byte[].
     * 
     * @param x
     * @return
     */
    public static byte[] intToBytes(int x) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putLong(x);
        return buffer.array();
    }
    
	public static void reverse(byte[] array) {
		if (array == null) {
			return;
		}
		int i = 0;
		int j = array.length - 1;
		byte tmp;
		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
	}
	
	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}
}
